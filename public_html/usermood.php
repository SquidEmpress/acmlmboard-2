<?php
  // 2009-07 Sukasa: The entire file.  I tried to conform to what I remembered of the standards.
  // 2011-09-09 Emuz: Small update to allow for <s>32</s> 64 avatars
  // 2011-10-01 Sukasa: Okay, I'll timestamp it that way then :P.  Updated to allow administrators to modify anyone's mood avatars, and some UI tweaks
  require 'lib/common.php';


  needs_login(1);

  $targetuserid = $loguser['id'];
  $target = false;
  $targetget = "";
  $targetgeta = "";

  $userid = isset($_GET['uid']) ? (int)$_GET['uid'] : $loguser['id'];

  if (CanAlterAll($userid) && isset($userid) && $userid != $loguser['id']) {
    $targetuserid = $userid;
    $target = true;
    $targetget = "&uid=".$targetuserid;
    $targetgeta = "?uid=".$targetuserid;
  }


  if (!can_edit_user_moods($targetuserid)) $targetuserid = 0;

  if ($targetuserid == 0) {
     error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>"); 
     pagefooter(); 
     die();
  }


  //Select existing avatar or new one
  $id = (isset($_GET['i']) ? $_GET['i'] : (isset($_POST['aid']) ? $_POST['aid'] : -1 ));
  $id = (int)$id;

  $activeavatar = $sql->fetchp("SELECT `id`, `label`, `url`, `local`, 1 `existing` FROM `mood` WHERE `user` = ? AND `id` = ? UNION SELECT 0 `id`, '(Label)' `label`, '' `url`, 1 `local`, 0 `existing`", array($targetuserid, $id));
  $avatars = $sql->prepare("SELECT * FROM `mood` WHERE `user`= ?", array($targetuserid));
  $numavatars = $sql->resultp("SELECT COUNT(*) FROM `mood` WHERE `user` = ?", array($targetuserid));

  if (isset($_POST['a']) && $_POST['a'][0] == 'D' && $activeavatar['existing']) {
    $sql->prepare("DELETE FROM mood WHERE id = ? and user = ?", array($id, $targetuserid));
    $avatars = $sql->prepare("SELECT * FROM `mood` WHERE `user` = ?", array($targetuserid));
  }

  if (isset($_POST['a']) && $_POST['a'][0] == 'S' && ($numavatars < 64 || $activeavatar['existing'])) {
    //vet the image
    $islocal = ($_POST['islocal'] != 'on' ? 1 : 0);
    $avatarid = ($activeavatar['existing'] == 1 ? $id : $sql->resultp("SELECT (id + 1) nid FROM `mood` WHERE user = ? UNION SELECT 1 nid ORDER BY nid DESC", array($targetuserid)));
    if($islocal && $fname = $_FILES['picture']['name']){
      $fext = strtolower(substr($fname, -4));
      $error = '';
      $exts = array('.png', '.jpg', '.gif');
      $dimx= 180;
      $dimy = 180;
      $dimxs = 60;
      $dimys = 60;
      $size = 2 * 61440;

	//[KAWA] TODO: replace with token effect
	/*
      if($x_hacks["180px"]) {
        $dimx = 180;
      	$dimy = 180;
	      $size = 61440;
      }
      */

      $validext = false;
      $extlist = '';
      foreach($exts as $ext){
        if($fext == $ext)
          $validext = true;
        $extlist .= ($extlist ? ', ' : '') . $ext;
      }
      if(!$validext)
        $error .= "<br>- Invalid file type, must be either: $extlist";

      if(($fsize = $_FILES['picture']['size']) > $size)
        $error .= "<br>- File size is too high, limit is $size bytes";

      if(!$error){
        $tmpfile = $_FILES['picture']['tmp_name'];
        $file = " userpic/" . $targetuserid . "_$avatarid";
        $file2 = "userpic/s" . $targetuserid . "_$avatarid";

        list($width, $height, $type) = getimagesize($tmpfile);

        if($type == 1) $img1 = imagecreatefromgif($tmpfile);
        if($type == 2) $img1 = imagecreatefromjpeg($tmpfile);
        if($type == 3) $img1 = imagecreatefrompng($tmpfile);

        if($type <= 3){
          $r = imagesx($img1)/imagesy($img1);
          $img2 = imagecreatetruecolor($dimxs, $dimys);
          imagecolorallocate($img2, 0, 0, 0);

          if($r > 1)
            imagecopyresampled($img2, $img1, 0, round($dimys * (1 - 1 / $r) / 2), 0, 0, $dimxs, $dimys / $r, imagesx($img1), imagesy($img1));
          else
            imagecopyresampled($img2, $img1, round($dimxs * (1 - $r) / 2), 0, 0, 0, $dimxs * $r, $dimys, imagesx($img1), imagesy($img1));
          imagepng($img2, $file2);
        }

        if($width <= $dimx && $height <= $dimy && $type <=3 )
          copy($tmpfile, "userpic/" . $targetuserid . "_$avatarid");
        elseif($type<=3){
          if($r > 1){
            $img2 = imagecreatetruecolor($dimx, $dimy / $r);
            imagecopyresampled($img2, $img1, 0, 0, 0, 0, $dimx, $dimy / $r, imagesx($img1), imagesy($img1));
          }else{
            $img2 = imagecreatetruecolor($dimx * $r, $dimy);
            imagecopyresampled($img2, $img1, 0, 0, 0, 0, $dimx * $r, $dimy, imagesx($img1), imagesy($img1));
          }
          imagepng($img2, $file);
          $usepic = 1;
        }else{
          print "<br>- Bad image format";
        }
        //Save the mood avatar
        $sql->prepare("INSERT INTO mood (id, user, url, local, label) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE url = ?, local = ?, label = ?", array($avatarid, $targetuserid , $_POST['url'], $islocal, $_POST['label'], $_POST['url'], $islocal, $_POST['label']));
        //and give it focus
        $id = $avatarid;
      }else
        noticemsg("Error", $error);
    }
 }  

  $activeavatar = $sql->fetchp("SELECT `id`,`label`, `url`, `local`, 1 `existing` FROM `mood` WHERE `user` =  ? AND `id` = ? UNION SELECT 0 `id`, '(Label)' `label`, '' `url`, 1 `local`, 0 `existing`", array($targetuserid, $id));
  $numavatars = $sql->resultp("SELECT COUNT(*) FROM `mood` WHERE `user` = ?", array($targetuserid));
  $avatars = $sql->prepare("SELECT * FROM `mood` WHERE `user` = ?", array($targetuserid));
  if ($target) {
    $targetname = $sql->resultp("SELECT `name` FROM `users` WHERE `id` = ?", array($targetuserid));
  }

  // Moved pageheader here so that I can do header()s without everything going haywire up again
  pageheader();


  print "<form id=\"f\" action=\"usermood.php$targetgeta\" enctype=\"multipart/form-data\" method=\"post\">
".      "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=250>
".      "      " . ($target ? $targetname . "'s" : "Your") . " current mood avatars ($numavatars)
".      "    </td>
".      "  <td class=\"b h\" align=\"center\" colspan='2'><nobr>
".      "    Add/change a mood avatar
".      "  </td>
".      "  <tr>
".      "    <td class=\"b n1\" style=\"vertical-align: top\" rowspan='4'>";

  while ($row = $sql->fetch($avatars))
    print "<a href=\"?a=e&i={$row['id']}$targetget\">" . stripslashes($row['label']) . "</a><br>";
  
  if ($numavatars < 64)
    print "          <a href=\"usermood.php$targetgeta\">(Add New)</a>";

  print "        </td>
".      "        <td class=\"b n2\"><nobr>
".      "          <input type=\"text\" style=\"width: 100%\" name=\"label\" value=\"" . stripslashes($activeavatar['label']) . "\">
".      "        <td class=\"b n2\">
".      "          <input type=\"submit\" name='a' value=\"Save\">
". ($id > 0 ?"           
" : "") .  "     <tr>
".      "       <td class=\"b n3\">
".      "          <input type=\"text\" style=\"width: 100%\" name=\"url\" value=\"" . stripslashes($activeavatar['url']) . "\">
".      "       <td class=\"b n3\" width='1'><nobr><input id=\"islocal\" name=\"islocal\" type=\"checkbox\"" . (!$activeavatar['local'] ? 'checked="checked"' : '') . "
".      "          <label for=\"islocal\">Use URL instead of uploaded file</label>
".      "     <tr>
".      "        <td class=\"b n2\">
".      "          <input type=\"file\" name=\"picture\"><input type=\"hidden\" name=\"aid\" id=\"aid\" value=\"{$activeavatar['id']}\">
". ($id > 0 ? "           <input type=\"submit\" name='a' value=\"Delete\">
" : "") .  "<input type=\"hidden\" name=\"aid\" id=\"aid\" value=\"{$activeavatar['id']}\"></td>
".      "        <td class=\"b n2\"><small>Limits: 180x180px, 60KB</small></td>
".      "     <tr>
".      "        <td class=\"b n2\" colspan='2'> " . ($activeavatar['id'] > 0 ? "<img src='gfx/userpic.php?id=" . $targetuserid . "_" . $activeavatar['id'] . "' title=\"" . stripslashes($activeavatar['label']) . "\">" : "&nbsp;") . "
".      "</table></form>
".      "<br>";

  pagefooter();

  
  function CanAlterAll($uid) {
    global $loguser;
    return can_edit_user_moods($uid);
  }

  ?>