<?php
  require 'lib/common.php';

  if($id = $_GET['id'])
    checknumeric($id);
  else $id = 0;

  $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
  
  if($page < 1) $page = 1;

  else if($id) {
    $user = $sql->fetchp("SELECT " . userfields() . " FROM users WHERE id = ?", array($id));
    if($user['id']) {
      
      $print = "<a href=./>Main</a> - Posts by user " . ($user['displayname'] ? $user['displayname'] : $user['name']) . "<br><br>
".           "<table cellspacing=\"0\" class=\"c1\">
".           "  <tr class=\"h\">
".           "    <td class=\"b h\">ID
".           "    <td class=\"b h\">Num.
".           "    <td class=\"b h\">Posted on
".           "    <td class=\"b h\">Thread title
".           "  </tr>";

      $numposts = $sql->fetchp("SELECT COUNT(*) c FROM posts WHERE user = ?", array($id));
      $numposts = $numposts['c'];

      $p = $sql->prepare("SELECT p.id pid, p.num, p.date, t.title, t.forum, t.announce, f.id, f.private fprivate, f.cat, c.private cprivate FROM (posts p LEFT JOIN threads t ON t.id = p.thread) "
                    ."LEFT JOIN forums f ON f.id = t.forum "
                    . "LEFT JOIN categories c ON c.id = f.cat WHERE p.user = ? "
                    ."ORDER BY p.num DESC LIMIT " . $sql->escape(($page-1) * $loguser['tpp']) . ", " . $loguser['tpp'], array($id));

      $i = 0;
      while($post = $sql->fetch($p)) {
        if(!(can_view_forum(array('id' => $post['id'], 'private' => $post['fprivate'], 'cat' => $post['cat'], 'cprivate' => $post['cprivate'])))) $tlink = "<i>(Restricted forum)</i>";
        else $tlink = "<a href=thread.php?pid={$post['pid']}#{$post['pid']}>{$post['title']}</a>";
        $print .= " ".(($i = !$i) ? "<tr class=\"n3\"" : "<tr class=\"n2\"").">
".              "  <td class=\"b\" align=\"center\">{$post['pid']}
".              "  <td class=\"b\" align=\"center\">#{$post['num']}
".              "  <td class=\"b\" align=\"center\">" . cdate($dateformat, $post['date']) . "
".              "  <td class=\"b\" align=\"center\">$tlink
".              "</tr>";
      }
      $print .= "</table>";

      if($numposts <= $loguser['tpp'])
        $fpagelist = '<br>';
      else{
        $fpagelist = 'Pages:';
        for($p = 1; $p <= 1+floor(($numposts-1)/$loguser['tpp']); $p++)
          if($p == $page)
            $fpagelist .= " $p";
          else
            $fpagelist .= " <a href=postsbyuser.php?id=$id&page=$p>$p</a>";
      }

    } else {
      $print="<table cellspacing=\"0\" class=\"c1\">
".           "  <tr class=\"n2\">
".           "    <td class=\"b n1\" align=\"center\">
".           "      This user does not exist.
".           "</table>";
    }
  } else {
    $print="<table cellspacing=\"0\" class=\"c1\">
".         "  <tr class=\"n2\">
".         "    <td class=\"b n1\" align=\"center\">
".         "      You must specify a user ID.
".         "</table>";
  }

  //This is heavily based off of AB1's code so posts by thread and posts by forum need to be cleaned at some point.
  if(isset($_GET['postsbythread'])) {
  $time = isset($_GET['time']) ? (int)$_GET['time'] : 86400;
  $posters = $sql->prepare("SELECT t.id, t.replies, t.title, t.forum, f.id, f.private fprivate, f.cat, c.private cprivate, COUNT(p.id) cnt FROM threads t, posts p, forums f, categories c WHERE p.user = ? AND p.thread = t.id AND p.date > ? AND t.forum = f.id AND c.id = f.cat GROUP BY t.id ORDER BY cnt DESC", array($id, (ctime()-$time)));
  $u = $sql->fetchp("SELECT " . userfields() . " FROM users WHERE id = ?", array($id));
  $username = ($u['displayname'] ? $u['displayname'] : $u['name']);
  if($time < 999999999) $during = ' during the last ' . timeunits2($time);
  $print = "Posts by $username in threads$during:
".      "<br>
".       timelink($time, 3600, "postsbyuser.php?postsbythread&id=$id&time") . '|' . timelink($time, 86400, "postsbyuser.php?postsbythread&id=$id&time") . '|' . timelink($time, 604800, "postsbyuser.php?postsbythread&id=$id&time") . '|' . timelink($time, 2592000, "postsbyuser.php?postsbythread&id=$id&time") . "
".           "<table cellspacing=\"0\" class=\"c1\">
".           "  <tr class=\"h\">
".	"<td class=\"b h\">#
".	"<td class=\"b h\">Thread
".	"<td class=\"b h\">Posts
".	"<td class=\"b h\">Thread total
".      "  </tr>
  ";
  for($i = 1; $t = $sql->fetch($posters); $i++){
    $print .= "
	<tr>
".	"<td class=\"b\" align=\"center\">$i</td>
".	"<td class=\"b\" align=left>
    ";
    if(!(can_view_forum(array('id' => $t['id'], 'private' => $t['fprivate'], 'cat' => $t['cat'], 'cprivate' => $t['cprivate']))))
	$print .= "<i>(Restricted forum)</i>";
    else $print .= "<a href=thread.php?id={$t['id']}>{$t['title']}</a>";
    $print .= "
	</td>
".	"<td class=\"b\" align=\"center\">{$t['cnt']}</td>
".     "<td class=\"b\" align=\"center\">" . ($t['replies']+1) . "</td>
".     "  </tr>
    ";
  }
      $print .= "</table>";
            $fpagelist = "";
  }
  
  if(isset($_GET['postsbyforum'])) {
  $time = isset($_GET['time']) ? (int)$_GET['time'] : 86400;

  if($id){
    $useridquery = "posts.user = ? AND";
    $useridparam = "$id, ";
    $by = 'by ';
    $u = $sql->fetchp("SELECT " . userfields() . " FROM users WHERE id = ?", array($id));
    $username = ($u['displayname'] ? $u['displayname'] : $u['name']);
  }
  $posters = $sql->prepare("SELECT forums.*, COUNT(posts.id) AS cnt FROM forums,threads,posts WHERE $useridquery posts.thread = threads.id AND threads.forum = forums.id AND posts.date > ? AND threads.announce = ? GROUP BY forums.id ORDER BY cnt DESC", array($useridparam . (ctime() - $time), 0));
  $userposts = $sql->prepare("SELECT id FROM posts WHERE $useridquery date > ?", array($useridparam . (ctime() - $time)));
  if($time < 999999999) $during = ' during the last ' . timeunits2($time);
  $print =  "Posts $by$username in forums$during:
".      "<br>
".       timelink($time, 3600, "postsbyuser.php?postsbyforum&id=$id&time") . '|' . timelink($time, 86400, "postsbyuser.php?postsbyforum&id=$id&time") . '|' . timelink($time, 604800, "postsbyuser.php?postsbyforum&id=$id&time") . '|' . timelink($time, 2592000, "postsbyuser.php?postsbyforum&id=$id&time") . "
".           "<table cellspacing=\"0\" class=\"c1\">
".           "  <tr class=\"h\">
".	"<td class=\"b h\">#
".	"<td class=\"b h\">Forum
".	"<td class=\"b h\">Posts
".	"<td class=\"b h\">Forum total
".      "  </tr>
  ";
  for($i = 1; $f = $sql->fetch($posters); $i++){
      if($i > 1) $print .= '<tr>';
	if(!(can_view_forum($f))) $link = "<i>(Restricted forum)</i>";
	else $link = "<a href=forum.php?id={$f['id']}>{$f['title']}</a>";
      $print .= "
".	"<td class=\"b\" align=\"center\">$i</td>
".	"<td class=\"b\">$link</td>
".	"<td class=\"b\" align=\"center\">{$f['cnt']}</td>
".	"<td class=\"b\" align=\"center\">{$f['posts']}</td>
".      "  </tr>
      ";
  }
      $print .= "</table>";
            $fpagelist = "";
  }
  
  if(isset($_GET['postsbytime'])) {
  $posttime = isset($_GET['time']) ? (int)$_GET['time'] : 86400;
  $time = ctime() - $posttime; 
  if($id){
    $user = $sql->fetchp("SELECT " . userfields() . " FROM users WHERE id = ?", array($id));
    $from = " from " . ($user['displayname'] ? $user['displayname'] : $user['name']);
  }else $from = ' on the board';
  if ($id) {
  $posts = $sql->prepare("SELECT count(*) AS cnt, FROM_UNIXTIME(date,'%k') AS hour FROM posts WHERE user = ? AND date > ? GROUP BY hour", array($id, $time));
  } else {
  $posts = $sql->prepare("SELECT count(*) AS cnt, FROM_UNIXTIME(date,'%k') AS hour FROM posts WHERE date > ? GROUP BY hour", array($time));
  }
  if($posttime < 999999999) $during = ' during the last ' . timeunits2($posttime);
  $print = "Posts$from by time of day$during:
".      "<br>
".       timelink($posttime, 3600, "postsbyuser.php?postsbytime&id=$id&time") . '|' . timelink($posttime, 86400, "postsbyuser.php?postsbytime&id=$id&time") . '|' . timelink($posttime, 604800, "postsbyuser.php?postsbytime&id=$id&time") . '|' . timelink($posttime, 2592000, "postsbyuser.php?postsbytime&id=$id&time") . "
".           "<table cellspacing=\"0\" class=\"c1\">
".           "  <tr class=\"h\">
".	"<td class=\"b h\" width=40>Hour
".	"<td class=\"b h\" width=50>Posts
".	"<td class=\"b h\">&nbsp<tr>";
  for($i = 0; $i < 24; $i++) $postshour[$i] = 0;
  while($h = $sql->fetch($posts)) $postshour[$h['hour']] = $h['cnt'];
  for($i = 0; $i < 24; $i++) if($postshour[$i] > $max) $max = $postshour[$i];
  for($i = 0; $i < 24; $i++){
    if($i) $print .=  '<tr>';
    $bar = "<img src=gfx/rpg/bar-on.png width=".(@floor($postshour[$i]/$max*10000)/100).'% height=8>';
    $print.= "
".	"<td class=\"b n2\">$i</td>
".	"<td class=\"b n2\">$postshour[$i]</td>
".	"<td class=\"b n2\" width=100%>$bar</td>
    ";
  }
      $print.="</table>";
            $fpagelist = "";
    }

  if(isset($_GET['postsbyforum'])) {
  if($id) pageheader("Posts in forums by user {$user['name']}");
  else pageheader("Posts in forums on the board");
  }
  else if(isset($_GET['postsbythread'])) {
  pageheader("Posts in threads by user {$user['name']}");
  }
  else if(isset($_GET['postsbytime'])) {
  if($id) pageheader("Posts by time of day from user {$user['name']}");
  else pageheader("Posts by time of day on the board");
  }
  else {
  pageheader("Posts by user {$user['name']}");
  }
  echo $print . $fpagelist . "<br>";
  pagefooter();
  
?>