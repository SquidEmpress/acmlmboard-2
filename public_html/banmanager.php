<?php
require 'lib/common.php';

//Alternative to editing users' profiles. - SquidEmpress
//Based off of banhammer.php from Blargboard by StapleButter.

$uid = $loguser['id'];
 
  if (isset($_GET['id'])) {
    $temp = $_GET['id'];
    if (checknumeric($temp))
      $uid = $temp;
  }

 if (!has_perm('ban-users'))
   {
     error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
   }
   
   //From editperms.php
   $id = (int)$_GET['id'];

    $tuser = $sql->fetchp("SELECT `group_id` FROM users WHERE id = ?", array($id));
	if ((is_root_gid($tuser['group_id']) || (!can_edit_user_assets($tuser['group_id']) && $id != $loguser['id'])) && !has_perm('no-restrictions')) 
	{
		error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
	}
 
   if($uid = $_GET['id']) {
     checknumeric($uid);
     $numid = $sql->fetchp("SELECT `id` FROM `users` WHERE `id` = ?", array($uid));
     if(!$numid) {
     error("Error", "Invalid user ID.");
    }
   }

global $user;
 
  $user = $sql->fetchp("SELECT * FROM users WHERE `id` = ?", array($uid));

if($_POST['banuser'] == "Ban User") {
      $tempban = ctime()+($_POST['tempbanned']);
      $tempban = "Banned until " . date("m-d-y h:i A", $tempban);
      if ($_POST['tempbanned'] > 0)
      {
      $banreason = $tempban;
      if ($_POST['title']) {
      $banreason .= ': ' . htmlspecialchars($_POST['title']); }
      }
      else
      {
      $banreason = "Banned permanently";
      if ($_POST['title']) {
      $banreason .= ': ' . htmlspecialchars($_POST['title']); }
      }

      $sql->prepare("UPDATE users SET group_id = ?, title = ?, tempbanned = ? WHERE id = ?", array(is_banned_gid('banned'), $banreason, ($_POST['tempbanned'] > 0 ? ($_POST['tempbanned']+time()) : 0), $user['id']));

         if ($boardlog == 1 || $boardlog >= 5) {
	         $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " banned " . userlink_by_id($user['id'], $config['showminipic']), $loguser['ip']));
             $id = $sql->insertid();
         }

                  redirect("profile.php?id={$user['id']}", -1);
die(pagefooter());
    }

elseif($_POST['unbanuser'] == "Unban User") {
if (!is_banned_gid($user['group_id']))
{
error("Error", "This user is not a Banned User.<br> <a href=./>Back to main</a> "); 
}
      $sql->prepare("UPDATE users SET group_id = ?, title = ?, tempbanned = ? WHERE id = ?", array(is_default_gid('default'), '', 0, $user['id']));

         if ($boardlog == 1 || $boardlog >= 5) {
	         $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " unbanned " . userlink_by_id($user['id'], $config['showminipic']), $loguser['ip']));
             $id = $sql->insertid();
         }

                  redirect("profile.php?id={$user['id']}", -2);
die(pagefooter());
    }

if (isset($_GET['unban']))
{
pageheader('Unban User');
}
else
{
pageheader('Ban User');
}

if (isset($_GET['unban']))
{
$pagebar = array
  (
	  'breadcrumb' => array(array('href' => '/.', 'title' => 'Main'), array('href' => 'index.php', 'title' => 'Forums')),
	  'title' => 'Unban User',
	  'actions' => array(),
  );
}
else
{
$pagebar = array
  (
	  'breadcrumb' => array(array('href' => '/.', 'title' => 'Main'), array('href' => 'index.php', 'title' => 'Forums')),
	  'title' => 'Ban User',
	  'actions' => array(),
  );
}
RenderPageBar($pagebar);
  
if (isset($_GET['unban']))
{
print "<form action='banmanager.php?id=$uid' method='post' enctype='multipart/form-data'> 
".    "<table cellspacing=\"0\" class=\"c1\">
".    "  <tr class=\"h\"><td class=\"b\">Unban User
".    "  <tr><td class=\"b n1\" align=\"center\">
".    "    <br>
".        "  <tr class=\"n1\">
".        "    <td class=\"b n1\" align=\"center\">
".        "      <input type=\"submit\" class=\"submit\" name=\"unbanuser\" value=\"Unban User\">
".    "</table>
";
}
else
{
print "<form action='banmanager.php?id=$uid' method='post' enctype='multipart/form-data'> 
".    "<table cellspacing=\"0\" class=\"c1\">
".
        catheader('Ban User')."
".        "  <tr>
".        "    <td class=\"b n1\" align=\"center\">Reason:</td>
".        "      <td class=\"b n2\"><input type=\"text\" name='title' class='right'></td>
".        "  <tr>
".        "    <td class=\"b n1\" align=\"center\">Expires?</td>
".        "      <td class=\"b n2\">".fieldselect("tempbanned", 0, array("600" => "10 minutes",
						      "3600" => "1 hour",
						      "10800" => "3 hours",
						      "86400" => "1 day",
						      "172800" => "2 days",
						      "259200" => "3 days",
						      "604800" => "1 week",
						      "1209600" => "2 weeks",
						      "2419200" => "1 month",
						      "4838400" => "2 months",
						      "0" => "never"))."</td>
".        "  <tr class=\"n1\">
".        "    <td class=\"b\">&nbsp;</td>
".        "    <td class=\"b\">
".        "      <input type=\"submit\" class=\"submit\" name=\"banuser\" value=\"Ban User\">
".    "</table>
";
}

pagefooter();
?>