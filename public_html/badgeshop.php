<?php
  require 'lib/common.php';

  /* Badge Shop
   * Based off of the Item Shop. Currently an unfinished prototype.
   * - SquidEmpress
   */

  $rdmsg = "";
  if(!empty($_COOKIE['pstbon'])) {
	header("Set-Cookie: pstbon=".$_COOKIE['pstbon']."; Max-Age=1; Version=1");
 $rdmsg = "<script language=\"javascript\">
	function dismiss()
	{
		document.getElementById(\"postmes\").style['display'] = \"none\";
	}
</script>
	<div id=\"postmes\" onclick=\"dismiss()\" title=\"Click to dismiss.\"><br>
".      "<table cellspacing=\"0\" class=\"c1\" width=\"100%\" id=\"edit\"><tr class=\"h\"><td class=\"b h\">";
if($_COOKIE['pstbon'] == -1) {
	$rdmsg .= "Badge Sold<div style=\"float: right\"><a style=\"cursor: pointer;\" onclick=\"dismiss()\">[x]</a></td></tr>
".	"<tr><td class=\"b n1\" align=\"left\">The badge has been unobtained and sold.</td></tr></table></div>";
} elseif($_COOKIE['pstbon'] == -2) {
	$rdmsg .= "Badge Bought<div style=\"float: right\"><a style=\"cursor: pointer;\" onclick=\"dismiss()\">[x]</a></td></tr>
".	"<tr><td class=\"b n1\" align=\"left\">The badge has been bought and obtained!</td></tr></table></div>"; }
}

  $action = isset($_GET['action']) ? $_GET['action'] : '';

  //needs_login(1);

$f = fopen("badgeshop-ref.log", "a");
fwrite($f, "[" . date("m-d-y H:i:s") . "] " . $ref . "\n");
fclose($f);

  /*if(!has_perm('use-badge-shop')){
     error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
  }*/

  if(!has_perm('no-restrictions')){
     error("Error", "The badge shop ain't finished yet!<br> <a href=./>Back to main</a>");
  }

    $user=$sql->fetchp('SELECT u.name, u.posts, u.regdate, r.* '
                      .'FROM users u '
                      .'LEFT JOIN usersrpg r ON u.id = r.id '
                      ."WHERE u.id = ?", array($loguser['id']));
    $p = $user['posts'];
    $d = (ctime()-$user['regdate']) / 86400;
    $st = getstats($user);
    $GP = $st['GP'];
    $sf = $sql->fetchp("SELECT * FROM user_badges WHERE user_id = ?", array($loguser['id']));

     if($action == "buy") {
        if(!strstr($ref,"badgeshop.php") || ctime()-$loguser['lastview'] < 1) die();

        $id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        $bbadge = $sql->fetchp("SELECT * FROM badges WHERE id = ?", array($id));

        if($bbadge['coins'] <= $GP && $bbadge['coins2'] <= 0 && $bbadge['id']) {
          $sql->prepare("UPDATE usersrpg "
                     ."SET spent = ? "
                     ."WHERE id = ?", array($user['spent']+$bbadge['coins'], $loguser['id']));
          $sql->prepare("INSERT INTO user_badges SET user_id = ?, badge_id = ?", array($loguser['id'], $bbadge['id']));
          $id = $sql->insertid();

                  redirect("badgeshop.php", -2);
        }
      }
      else if($action == "sell") {
        $id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        $sbadge = $sql->fetchp("SELECT * FROM badges "
                           ."WHERE id = ?", array($id));
        $sql->prepare("UPDATE usersrpg "
                   ."SET spent = ? "
                   ."WHERE id = ?", array($user['spent']-$sbadge['coins'], $loguser['id']));
        $sql->prepare("DELETE FROM user_badges WHERE user_id = ? AND badge_id = ?", array($loguser['id'], $sbadge['id']));

                  redirect("badgeshop.php", -1);
      }

  pageheader('Badge shop');
        print  "<script>
".            "  function preview(user,badge,name){
".            "    document.getElementById('prev').src='gfx/status.php?u='+user+'&ba='+badge+'&'+Math.random();
".            "    document.getElementById('pr').innerHTML='With<br>'+name+'<br>---------->';
".            "  }
".            "</script>
".            "<style>
".            "   .disabled {color:#888888}
".            "   .higher   {color:#abaffe}
".            "   .equal    {color:#ffea60}
".            "   .lower    {color:#ca8765}
".            "</style>
".            "<br>
".            "<table cellspacing=\"0\" id=status>
".            "  <td class=\"nb\" width=256><img src=gfx/status.php?u={$loguser['id']}></td>
".            "  <td class=\"nb\" align=\"center\" width=150>
".            "    <font class=fonts>
".            "      <div id=pr></div>
".            "    </font>
".            "  </td>
".            "  <td class=\"nb\">
".            "    <img src=img/_.png id=prev>
".            "</table>
".            "<br>
";

        $badges  =$sql->query ('SELECT * FROM badges WHERE type = 2 ORDER BY id, coins');

        if(!empty($_COOKIE['pstbon'])) print $rdmsg;
        print "<table cellspacing=\"0\" class=\"c1\">
".            "  <tr class=\"h\">
".            "    <td class=\"b h\" width=100>Commands</td>
".            "    <td class=\"b n2\" width=1 rowspan=10000>&nbsp;</td>
".            "    <td class=\"b h\" width=40>Image</td>
".            "    <td class=\"b h\">Badge</td>
".            "    <td class=\"b h\">Description</td>
".            "    <td class=\"b h\" width=6%><img src=img/coin.gif></td>
".            "    <td class=\"b h\" width=6%><img src=img/coin2.gif></td>
";

        while($badge=$sql->fetch($badges)) {
          $preview = "<a href=#status onclick=\"preview({$loguser['id']},{$badge['id']},'".addslashes($badge['name'])."')\">Preview</a>";
          if (has_perm('edit-badges')) {
              $buy = "<a href=badgeshop.php?action=buy&id={$badge['id']}>Buy</a> | <a href=editbadges.php?action=edit&id={$badge['id']}>Edit</a> | $preview";
              $sell = "<a href=badgeshop.php?action=sell&id={$badge['id']}>Sell</a> | <a href=editbadges.php?action=edit&id={$badge['id']}>Edit</a>";
              $ps = "<a href=editbadges.php?action=edit&id={$badge['id']}>Edit</a> | $preview";
          } else{ 
              $buy = "<a href=badgeshop.php?action=buy&id={$badge['id']}>Buy</a> | $preview";
              $sell = "<a href=badgeshop.php?action=sell&id={$badge['id']}>Sell</a>";
              $ps = $preview;
          }

          if($badge['id'] && $badge['id'] == $sf['badge_id']) $comm = $sell;
          elseif($badge['id'] && $badge['coins'] <= $GP && $badge['coins2'] <= 0) $comm = "$buy";
          else                                   $comm = $ps;

          if($badge['id'] == $sf['badge_id']) $color = ' class=equal';
          elseif($badge['coins'] > $GP)   $color = ' class=disabled';
          else                       $color = '';

         print
              "  <tr$color>
".            "    <td class=\"b n2\" align=\"center\">$comm</td>
".            "    <td class=\"b n1\"><img src=\"{$badge['image']}\" alt=\"\" /></td>
".            "    <td class=\"b n1\" align=\"center\">{$badge['name']}</td>
".            "    <td class=\"b n1\" align=\"center\">{$badge['description']}</td>
".            "    <td class=\"b n1\" align=\"right\">{$badge['coins']}</td>
".            "    <td class=\"b n1\" align=\"right\">{$badge['coins2']}</td>
";
        }
        print "</table>
";

  pagefooter();
?>