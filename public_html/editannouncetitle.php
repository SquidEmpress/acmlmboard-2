<?php
//Uses editpost.php as template

  require 'lib/common.php';

  if($act = isset($_POST['action']))
  {
    $pid = (int)$_POST['pid'];  

  }
  else
  {
    $pid = isset($_GET['pid']) ? (int) $_GET['pid'] : 0;
  }

  $thread = $sql->fetchp('SELECT p.user puser, t.*, f.title ftitle, f.private fprivate, f.readonly freadonly '
                      .'FROM posts p '
                      .'LEFT JOIN threads t ON t.id = p.thread '
                      .'LEFT JOIN forums f ON f.id = t.forum '
                      ."WHERE p.id = ? AND t.announce = 1 AND (t.forum IN " . forums_with_view_perm() . " OR (t.forum IN (0, NULL) AND t.announce >= 1))", array($pid));


  if (!$thread) $pid = 0;
if($act != "Submit"){
  echo "<script language=\"javascript\" type=\"text/javascript\" src=\"tools.js\"></script>";
}
else if (!can_edit_post(array('user' => $thread['puser'], 'tforum' => $thread['forum']))) {
      error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
  }
  elseif($pid == -1){
      error("Error", "Your PID code is invalid!<br> <a href=./>Back to main</a>");
  }

  $top = '<a href=./>Main</a> '
    .($thread['forum'] == 0 ? "- <a href=thread.php?announce=0>Announcements</a> " : "- <a href=forum.php?id={$thread['forum']}>{$thread['ftitle']}</a> ")
    .'- Edit announcement title';

  $res = $sql->prepare("SELECT u.id, p.user, p.mood, p.nolayout, pt.text "
                    ."FROM posts p "
                    ."LEFT JOIN poststext pt ON p.id = pt.id "
                    ."JOIN ("
                      ."SELECT id, MAX(revision) toprev FROM poststext GROUP BY id"
                    .") as pt2 ON pt2.id = pt.id AND pt2.toprev = pt.revision "
                    ."LEFT JOIN users u ON p.user = u.id "
                    ."WHERE p.id = ?", array($pid));

  if(@$sql->numrows($res) < 1){
    error("Error", "That post does not exist.<br> <a href=./>Back to main</a>");
    }


  $post = $sql->fetch($res);
if(!$act){
  pageheader('Edit announcement title', $thread['forum']);
    print "$top
".        "<br><br>
".        "<table cellspacing=\"0\" class=\"c1\">
".        " <form action=editannouncetitle.php method=post>
".        "  <tr class=\"h\">
".        "    <td class=\"b h\" colspan=2>Edit Announcement Title</td>
".        "  <tr>
".        "    <td class=\"b n1\" align=\"center\">Title:</td>
".        "    <td class=\"b n2\"><input type=\"text\" name=title size=100 maxlength=100 value='" . $thread['title'] . "' class='right'></td>
".        "  <tr class=\"n1\">
".        "    <td class=\"b\">&nbsp;</td>
".        "    <td class=\"b\">
".        "      <input type=\"hidden\" name=pid value=$pid>
".        "      <input type=\"submit\" class=\"submit\" name=action value=Submit>
".        "    </td>
".        " </form>
".        "</table>
";
  }elseif($act == 'Submit'){
         if ($boardlog == 2 || $boardlog >= 5) {
	         if($thread['forum'] > 0) $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " renamed <a href='thread.php?pid=$pid'>{$thread['title']}</a> (<a href='forum.php?id={$thread['forum']}'>{$thread['ftitle']}</a>)", $loguser['ip']));
	         else $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " renamed <a href='thread.php?pid=$pid'>{$thread['title']}</a>", $loguser['ip']));
             $id = $sql->insertid();
         }
    $sql->prepare("UPDATE threads SET title = ? WHERE id = ?", array($_POST['title'], $thread['id']));

  redirect("thread.php?pid=$pid#edit", "-1");
  }

  pagefooter();
?>