<?php
include('lib/common.php');
include('lib/diff/Diff.php');
include('lib/diff/Diff/Renderer/inline.php');

$pid = isset($_GET['id']) ? (int)$_GET['id'] : 0;
$r1 = isset($_GET['o']) ? (int)$_GET['o'] : 0;
$r2 = isset($_GET['n']) ? (int)$_GET['n'] : 0;

$t = $sql->resultp("SELECT thread FROM posts WHERE id = ?", array($pid));
if(!$t) { error("Error", "This post does not exist.<br> <a href=./>Back to main</a>"); }
$f = $sql->resultp("SELECT forum FROM threads WHERE id = ?", array($t));
if(!can_view_forum_post_history($f)) { error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>"); }

pageheader("Post revision differences");

if(!$r1 || !$r2) $r1 = $r2=1;

$d = $sql->fetchp("SELECT p1.text d1, p2.text d2 FROM poststext p1 LEFT JOIN poststext p2 ON p2.id = ? AND p2.revision = ? WHERE p1.id = ? AND p1.revision = ?", array($pid, $r2, $pid, $r1));

echo "<table cellspacing=\"0\" class=\"c1\" width=100% height=100><tr class=\"n1\"><td class=\"b n2\"><font face='courier new'>";

$diff = new Text_Diff("native", array(explode("\n", $d['d1']), explode("\n", $d['d2'])));

?>
<style type=text/css>
del {
	text-decoration: none;
	background-color: #800000;
	border: 1px dashed #FF0000;
	color: #cfcfcf;
	margin-left:1px;
	padding-left:1px;
	padding-right:1px;
}
ins {
	text-decoration: none;
	background-color: #008000;
	border: 1px dashed #00FF00;
	color: #ffffff;
	margin-left:1px;
	padding-left:1px;
	padding-right:1px;
}
</style>
<?php

$renderer = new Text_Diff_Renderer_inline();
//What is this? I don't even…
/*if($act=="hs") {
	$ip=$sql->fetchq("SELECT ip FROM users WHERE id=$pid");
	$ip=$ip[0];
	echo $ip." = ".gethostbyaddr($ip);
} else*/ echo str_replace("\n", "<br>", $renderer->render($diff));

//echo diff(str_replace("\n","<br>\n",$d1[text])."\n",str_replace("\n","<br>\n",$d2[text])."\n");
echo "</table>";

pagefooter();

?>