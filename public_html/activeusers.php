<?php
  require 'lib/common.php';
  pageheader('Active users');

  $time = isset($_GET['time']) ? (int) $_GET['time'] : 86400;
  if($time < 1)
    $time = 86400;

  $query = 'SELECT '.userfields('u').', u.posts, u.regdate, COUNT(*) num '
        .'FROM users u '
        .'LEFT JOIN posts p ON p.user = u.id '
        .'WHERE p.date > ? '
        .'GROUP BY u.id ORDER BY num DESC';
  $users = $sql->prepare($query, array((ctime()-$time)));

  print 'Active users during the last ' . timeunits2($time) . ":
".      "<br>
".       timelink($time, 3600, "activeusers.php?time") . '|' . timelink($time, 86400, "activeusers.php?time") . '|' . timelink($time, 604800, "activeusers.php?time") . '|' . timelink($time, 2592000, "activeusers.php?time") . "
".      "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=30>#</td>
".      "    <td class=\"b h\">Username</td>
".      "    <td class=\"b h\" width=150>Registered on</td>
".      "    <td class=\"b h\" width=50>Posts</td>
".      "    <td class=\"b h\" width=50>Total</td>
";
  $post_total = 0;
  $post_overall = 0;
  $j = 0;
  $tr = 'n3';
  for($i = 1; $user = $sql->fetch($users); $i++){
    $post_total += $user['num'];
    $post_overall += $user['posts'];
    $tr = ($i % 2 ? 'n2': 'n3');
    print
        "<tr class=\"$tr\" align=\"center\">
".      "    <td class=\"b\">$i.</td>
".      "    <td class=\"b\" align=\"left\">" . userlink($user) . "</td>
".      "    <td class=\"b\">" . cdate($dateformat, $user['regdate']) . "</td>
".      "    <td class=\"b\"><b>{$user['num']}</b></td>
".      "    <td class=\"b\">${user['posts']}</b></td>
";
  $j++;
  }
  print "<tr class=\"h\"><td class=\"b h\" colspan=5>Totals</td></tr>
".        "<tr class=\"$tr\" align=\"center\">
".      "    <td class=\"b\"><b>$j.</b></td>
".      "    <td class=\"b\" align=\"left\"></td>
".      "    <td class=\"b\"></td>
".      "    <td class=\"b\"><b>$post_total</b></td>
".      "    <td class=\"b\"><b>$post_overall</b></td>
";
  print "</table>
";

  pagefooter();

?>