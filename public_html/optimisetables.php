<?php

require 'lib/common.php';

if (!has_perm('optimise-tables'))
	error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");

pageheader('Optimise Tables');

  function df($input){
    $_df=number_format($input,0,'.',' ');
    return $_df;
  }

print "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=34%>Table name</td>
".      "    <td class=\"b h\"  width=22%>Rows</td>
".      "    <td class=\"b h\" width=22%>Unused data</td>
".      "    <td class=\"b h\" width=22%>Status</td>
";

  $tstats=$sql->query("SHOW TABLE STATUS");
  while($table=$sql->fetch($tstats)) {
             $status = "OK";
             if($table['Data_free'] > 0) {
               $sql->query("OPTIMIZE TABLE `{$table['Name']}`");
               $status = "OK (Table has been optimised)";
             }
print "  <tr align=\"right\">
".      "    <td class=\"b n1\" align=\"center\" >{$table['Name']}</td>
".      "    <td class=\"b n2\" align=\"center\" >{$table['Rows']}</td>
".      "    <td class=\"b n2\" align=\"center\" >".df($table['Data_free'])."</td>
".      "    <td class=\"b n2\" align=\"center\" >{$status}</td>
";
  }

print "</table>
";

pagefooter();
?>