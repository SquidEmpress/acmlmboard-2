<?php
  /* editpost.php ****************************************
    Changelog
0308  blackhole89   forked from newreply.php
0221  blackhole89       related to thread-individual "NEW" display system
0220  blackhole89       added minpower check for displaying the thread's
                        previous contents. (yes, it is possible to make a forum
                        with minpowerreply < minpower and allow users to "reply blindly" now)
  */

  require 'lib/common.php';
  require 'lib/threadpost.php';
  loadsmilies();

  $action = isset($_GET['act']) ? $_GET['act'] : '';
  $err = '';
  $act = isset($_POST['action']) ? $_POST['action']: '';
  if($act)
  {
    $pid = $_POST['pid'];  
	
	if ($_POST['passenc'] !== md5($pwdsalt2 . $loguser['pass'] . $pwdsalt))
		$err = 'Invalid token.';
  }
  else
  {
    $pid = isset($_GET['pid']) ? (int)$_GET['pid']: 0;
  }
  
  $userid = $loguser['id'];
  $user = $loguser;
  $pass = md5($pwdsalt2 . $loguser['pass'] . $pwdsalt);

  if($action == 'delete' || $action == 'undelete') {
    $act = $action;
    $pid = unpacksafenumeric($pid);
  }

  checknumeric($pid);

  $thread = $sql->fetchp('SELECT p.user puser, t.*, f.title ftitle, f.private fprivate, f.readonly freadonly, f.cat, c.private cprivate '
                      .'FROM posts p '
                      .'LEFT JOIN threads t ON t.id=p.thread '
                      .'LEFT JOIN forums f ON f.id = t.forum '
		      . 'LEFT JOIN categories c ON c.id = f.cat '
                      ."WHERE p.id = ? AND (t.forum IN ".forums_with_view_perm()." OR (t.forum IN (0, NULL) AND t.announce >= 1))", array($pid));

  if (!$thread) $pid = 0;
if($act != "Submit") { //Classical Redirect
  echo "<script language=\"javascript\" type=\"text/javascript\" src=\"tools.js\"></script>";
}
  $toolbar = posttoolbar();

  if ($thread['closed'] && !can_edit_forum_posts($thread['forum'])) {
      $err = "    You can't edit a post in closed threads!<br>
".         "    $threadlink";
  }
  else if (!can_edit_post(array('user'=>$thread['puser'], 'tforum' => $thread['forum']))) {
      $err = "    You do not have permission to edit this post.<br>$threadlink";
  }
  elseif($pid==-1){
      $err = "    Your PID code is invalid!<br>
".         "    $threadlink";
  }

  if($act=='Submit'){
    //2007-02-19 //blackhole89 - table breakdown protection
    if(($tdepth = tvalidate($message)) != 0)
      $err = "    This post would disrupt the board's table layout! The calculated table depth is $tdepth.<br>
".         "    $threadlink";
  }

  $top='<a href=./>Main</a> '
    .($thread['announce'] && $thread['forum'] == 0 ? "- <a href=thread.php?announce=0>Announcements</a> " : "- <a href=forum.php?id={$thread['forum']}>{$thread['ftitle']}</a> ")
    .($thread['announce'] && $thread['forum'] == 0 ? "- " . htmlval($thread['title']) . " " : "- <a href=thread.php?id={$thread['id']}>" . htmlval($thread['title']) . '</a> ')
    .'- Edit post';

  $res = $sql->prepare("SELECT u.id, p.user, p.mood, p.nolayout, p.nosmilies, pt.text "
                    ."FROM posts p "
                    ."LEFT JOIN poststext pt ON p.id = pt.id "
                    ."JOIN ("
                      ."SELECT id,MAX(revision) toprev FROM poststext GROUP BY id"
                    .") as pt2 ON pt2.id = pt.id AND pt2.toprev = pt.revision "
                    ."LEFT JOIN users u ON p.user=u.id "
                    ."WHERE p.id = ?", array($pid));

  if(@$sql->numrows($res) < 1)
    $err = "    That post does not exist.";

  $post = $sql->fetch($res);
  $quotetext = htmlval($post['text']);
if($act == "Submit" && $post['text'] == $_POST['message']) {
      $err = "    No changes detected.<br>
".         "    $threadlink";
}

  if($err){
if($loguser['redirtype'] == 1 && $act == "Submit") { pageheader('Edit post', $thread['forum']); }
  pageheader('Edit post', $thread['forum']);
    print "$top - Error";
    noticemsg("Error", $err);
  }elseif(!$act) {
  pageheader('Edit post', $thread['forum']);
    print "$top
".        "<br><br>
".        "<table cellspacing=\"0\" class=\"c1\">
".        " <form action=editpost.php method=post>
".        "  <tr class=\"h\">
".        "    <td class=\"b h\" colspan=2>Edit Post</td>
";
    print "  <input type=\"hidden\" name=name value=\"" . htmlval($loguser['name']) . "\">
".        "  <input type=\"hidden\" name=passenc value=\"$pass\">
";
    if($loguser['posttoolbar'] == 0 || $loguser['posttoolbar'] == 1)
    print "  <tr>
".        "    <td class=\"b n1\" align=\"center\" width=120>Format:</td>
".        "    <td class=\"b n2\"><table cellspacing=\"0\"><tr>$toolbar</table>
";
print     "  <tr>
".        "    <td class=\"b n1\" align=\"center\" width=120>Post:</td>
".        "    <td class=\"b n2\"><textarea wrap=\"virtual\" name=message id='message' rows=20 cols=80>$quotetext</textarea></td>
".        "  <tr class=\"n1\">
".        "    <td class=\"b\">&nbsp;</td>
".        "    <td class=\"b\">
".        "      <input type=\"hidden\" name=pid value=$pid>
".        "      <input type=\"submit\" class=\"submit\" name=action value=Submit>
".        "      <input type=\"submit\" class=\"submit\" name=action value=Preview>
".        "      <select name=mid>" . moodlist($post['mood'], $post['user']) . "
".        "      <input type=\"checkbox\" name=nolayout id=nolayout value=1 " . ($post['nolayout'] ? "checked" : "") . "><label for=nolayout>Disable post layout</label>
".        "      <input type=\"checkbox\" name=nosmilies id=nosmilies value=1 " . ($post['nosmilies'] ? "checked" : "") . "><label for=nosmilies>Disable smilies</label>
";
    if(can_edit_forum_threads($thread['forum']) && !$thread['announce'])
    print "     " . (!$thread['closed'] ? "<input type=\"checkbox\" name=close id=close value=1 " . (isset($_POST['close']) ? "checked" : "") . "><label for=close>Close thread</label>" : "")."
                 ".(!$thread['sticky'] ? "<input type=\"checkbox\" name=stick id=stick value=1 " . (isset($_POST['stick']) ? "checked" : "") . "><label for=stick>Stick thread</label>" : "")."
                 ".($thread['closed'] ? "<input type=\"checkbox\" name=open id=open value=1 " . (isset($_POST['open']) ? "checked" : "") . "><label for=open>Open thread</label>" : "")."
                 ".($thread['sticky'] ? "<input type=\"checkbox\" name=unstick id=unstick value=1 " . (isset($_POST['unstick']) ? "checked" : "") . "><label for=unstick>Unstick thread</label>" : "")."
";
    print "    </td>
".        " </form>
".        "</table>
";
  }elseif($act == 'Preview') {
    $_POST['message'] = stripslashes($_POST['message']);
    $euser = $sql->fetchp("SELECT * FROM users WHERE id = ?", array($post['id']));
    $post['date'] = ctime();
    $post['ip'] = $userip;
    $post['num'] = ++$euser['posts'];
    $post['mood'] = (isset($_POST['mid']) ? (int)$_POST['mid'] : -1);
    $post['nolayout'] = (isset($_POST['nolayout']) ? (int)$_POST['nolayout'] : 0);
    $post['nosmilies'] = (isset($_POST['nosmilies']) ? (int)$_POST['nosmilies'] : 0);
    $post['close'] = (isset($_POST['close']) ? (int)$_POST['close'] : 0);
    $post['stick'] = (isset($_POST['stick']) ? (int)$_POST['stick'] : 0);
    $post['open'] = (isset($_POST['open']) ? (int)$_POST['open'] : 0);
    $post['unstick'] = (isset($_POST['unstick']) ? (int)$_POST['unstick'] : 0);
    $post['text'] = $_POST['message'];
    foreach($euser as $field => $val)
      $post['u' . $field] = $val;
    $post['ulastpost'] = ctime();

  pageheader('Edit post', $thread['forum']);
    print "$top - Preview
".        "<br>
".        "<table cellspacing=\"0\" class=\"c1\">
".        "  <tr class=\"h\">
".        "    <td class=\"b h\" colspan=2>Post preview
".        "</table>
".         threadpost($post, 0)."
".        "<br>
".        "<table cellspacing=\"0\" class=\"c1\">
".        " <form action=editpost.php method=post>
".        "  <tr class=\"h\">
".        "    <td class=\"b h\" colspan=2>Post</td>
";
     if($loguser['posttoolbar'] == 0 || $loguser['posttoolbar'] == 1)
print     "  <tr>
".        "    <td class=\"b n1\" align=\"center\" width=120>Format:</td>
".        "    <td class=\"b n2\"><table cellspacing=\"0\"><tr>$toolbar</table>
";
print     "  <tr>
".        "    <td class=\"b n1\" align=\"center\" width=120>Post:</td>
".        "    <td class=\"b n2\"><textarea wrap=\"virtual\" name=message id='message' rows=10 cols=80>" . htmlval($_POST['message']) . "</textarea></td>
".        "  <tr class=\"n1\">
".        "    <td class=\"b\">&nbsp;</td>
".        "    <td class=\"b\">
".        "      <input type=\"hidden\" name=name value=\"" . htmlval(stripslashes($_POST['name'])) . "\">
".        "      <input type=\"hidden\" name=passenc value=\"$pass\">
".        "      <input type=\"hidden\" name=pid value=$pid>
".        "      <input type=\"submit\" class=\"submit\" name=action value=Submit>
".        "      <input type=\"submit\" class=\"submit\" name=action value=Preview>
".        "      <select name=mid>" . moodlist($post['mood'], $post['user'])."
".        "      <input type=\"checkbox\" name=nolayout id=nolayout value=1 " . ($post['nolayout'] ? "checked" : "") . "><label for=nolayout>Disable post layout</label>
".        "      <input type=\"checkbox\" name=nosmilies id=nosmilies value=1 " . ($post['nosmilies'] ? "checked" : "") . "><label for=nosmilies>Disable smilies</label>
";
    if(can_edit_forum_threads($thread['forum']) && !$thread['announce'])
    print "     " . (!$thread['closed'] ? "<input type=\"checkbox\" name=close id=close value=1 " . ($post['close'] ? "checked" : "") . "><label for=close>Close thread</label>" : "")."
                 ".(!$thread['sticky'] ? "<input type=\"checkbox\" name=stick id=stick value=1 " . ($post['stick'] ? "checked" : "") . "><label for=stick>Stick thread</label>" : "")."
                 ".($thread['closed'] ? "<input type=\"checkbox\" name=open id=open value=1 " . ($post['open'] ? "checked" : "") . "><label for=open>Open thread</label>" : "")."
                 ".($thread['sticky'] ? "<input type=\"checkbox\" name=unstick id=unstick value=1 " . ($post['unstick'] ? "checked" : "") . "><label for=unstick>Unstick thread</label>" : "")."
";
    print "    </td>
".        " </form>
".        "</table>
";
  }elseif($act == 'Submit') {
    $message = $_POST['message'];
    $user=$sql->fetchp("SELECT * FROM users WHERE id = ?", array($userid));

    $rev = $sql->fetchp("SELECT MAX(revision) m FROM poststext WHERE id = ?", array($pid));
    $rev = $rev['m'];
    $mid = (isset($_POST['mid']) ? (int)$_POST['mid'] : -1);
    checknumeric($mid);
    checknumeric($_POST['nolayout']);
    checknumeric($_POST['nosmilies']);
    if(can_edit_forum_threads($thread['forum'])){
    	checknumeric($_POST['close']);
    	checknumeric($_POST['stick']);
        checknumeric($_POST['open']);
        checknumeric($_POST['unstick']);
   	if($_POST['close']) $modext = ",closed=1";
	if($_POST['stick']) $modext .= ",sticky=1";
  	if($_POST['open']) $modext = ",closed=0";
	if($_POST['unstick']) $modext .= ",sticky=0";
    }
    ++$rev;
    $sql->prepare("INSERT INTO poststext (id, text, revision, user, date) VALUES (?, ?, ?, ?, ?)", array($pid, $message, $rev, $userid, ctime()));
    $sql->prepare("UPDATE posts SET mood = ?, nolayout = ?, nosmilies = ? WHERE id = ?", array($mid, $_POST['nolayout'], $_POST['nosmilies'], $pid));
    $sql->prepare("UPDATE threads SET lastdate = ?, lastuser = ?, lastid = ?$modext WHERE id = ?", array(ctime(), $userid, $pid, $thread['id'])); //Just an FYI, this DOES NOT "fuck up" the lastpost column of threads.
    $sql->prepare("UPDATE forums SET lastdate = ?, lastuser = ?, lastid = ? WHERE id = ?", array(ctime(), $userid, $pid, $thread['forum'])); //It was intentionally made this way as a convenience. If a user were to edit a post in a thread that is not the last post and they wanted everyone to know that the post was edited, how are they supposed to let everyone know it was edited? Post a new reply saying "I edited this post in this thread"? Sounds like a wasteful post to make… This is especially important for thread creators so that they have a way of letting other users know that they edited the first post in the thread.
    
    if($config['log'] >= '2') $sql->prepare("INSERT INTO log VALUES (UNIX_TIMESTAMP(), ?, ?, ?)", array($_SERVER['REMOTE_ADDR'], $loguser['id'], "ACTION: post edit $pid rev $rev"));

    $chan = $sql->resultp("SELECT a.chan FROM forums f LEFT JOIN announcechans a ON f.announcechan_id = a.id WHERE f.id = ?", array($thread['forum']));

    if ($thread['announce']) {
      if ($thread['forum'] == 0) {
    sendirc("{irccolor-base}Announcement edited by {irccolor-name}" . get_irc_displayname() . "{irccolor-url} ({irccolor-title}{$thread['title']}{irccolor-url}){irccolor-base} - {irccolor-url}{boardurl}?p=$pid{irccolor-base}", $config['pubchan']);

         if ($boardlog == 2 || $boardlog >= 5) {
	         $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  "Announcement edited by " . userlink_by_id($loguser['id'], $config['showminipic']) . " in <a href='?p=$pid'>{$thread['title']}</a>", $loguser['ip']));
             $id = $sql->insertid();
         }

      }
      else {
    sendirc("{irccolor-base}Announcement edited by {irccolor-name}" . get_irc_displayname() . "{irccolor-url} ({irccolor-title}{$thread['ftitle']}{irccolor-url}: {irccolor-name}{$thread['title']}{irccolor-url}){irccolor-base} - {irccolor-url}{boardurl}?p=$pid{irccolor-base}", $chan);

         if ($boardlog == 2 || $boardlog >= 5) {
	         $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  "Announcement edited by " . userlink_by_id($loguser['id'], $config['showminipic']) . " in <a href='?p=$pid'>{$thread['title']}</a> (<a href='forum.php?id={$thread['forum']}'>{$thread['ftitle']}</a>)", $loguser['ip']));
             $id = $sql->insertid();
         }

      }
    }
    else {
    sendirc("{irccolor-base}Post edited by {irccolor-name}" . get_irc_displayname() . "{irccolor-url} ({irccolor-title}{$thread['ftitle']}{irccolor-url}: {irccolor-name}{$thread['title']}{irccolor-url} ({irccolor-base}\x02\x02{$thread['id']}{irccolor-url})){irccolor-base} - {irccolor-url}{boardurl}?p=$pid{irccolor-base}", $chan);

         if ($boardlog == 2 || $boardlog >= 5) {
	         $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  "Post edited by " . userlink_by_id($loguser['id'], $config['showminipic']) . " in <a href='?p=$pid'>{$thread['title']}</a> (<a href='forum.php?id={$thread['forum']}'>{$thread['ftitle']}</a>)", $loguser['ip']));
             $id = $sql->insertid();
         }

    }
  redirect("thread.php?pid=$pid#edit", "-1");
  }elseif($act == 'delete' ||$act == 'undelete') {
    if(!(can_delete_forum_posts($thread['forum']))) {
  pageheader('Edit post', $thread['forum']);
      print "$top - Error";
      noticemsg("Error", "You do not have the permission to do this.");
    } else {
      $sql->prepare("UPDATE posts SET deleted = " .($act == 'delete' ? 1 : 0) . " WHERE id = ?", array($pid));
  redirect("thread.php?pid=$pid#edit", -1);
    }
  }
  //Shamelessly taken from newreply.php - SquidEmpress
  if($act != 'Submit' && $act != 'delete' && $act != 'undelete' && !$err && !$thread['announce'] && can_view_forum(array('id' => $thread['forum'], 'private' => $thread['fprivate'], 'cat' => $thread['cat'], 'cprivate' => $thread['cprivate']))){
    $posts = $sql->prepare("SELECT " . userfields('u', 'u') . ", u.posts AS uposts, u.regdate AS uregdate, p.*, pt1.text, t.forum tforum "
                      .'FROM posts p '
					  .'LEFT JOIN threads t ON t.id = p.thread '
                      .'LEFT JOIN poststext pt1 ON p.id = pt1.id '
                      .'LEFT JOIN poststext pt2 ON pt2.id = pt1.id AND pt2.revision = (pt1.revision+1) '
                      .'LEFT JOIN users u ON p.user = u.id '
                      ."WHERE p.thread = ? "
                      ."  AND ISNULL(pt2.id) "
                      .'ORDER BY p.id DESC '
                      ."LIMIT {$loguser['ppp']}", array($thread['id']));
    print "<br>
".        "<table cellspacing=\"0\" class=\"c1\">
".        "  <tr class=\"h\">
".        "    <td class=\"b h\" colspan=2>Thread preview
".        "</table>
";
    while($post = $sql->fetch($posts)) {
      $exp = calcexp($post['uposts'], ctime() - $post['uregdate']);
      print threadpost($post, 1);
    }

    if($thread['replies'] >= $loguser['ppp']){
    print "<br>
".        "<table cellspacing=\"0\" class=\"c1\">
".        "  <tr>
".        "    <td class=\"b n1\">The full thread can be viewed <a href=thread.php?id={$thread['id']}>here</a>.
".        "</table>
";
    }
}


  pagefooter();
?>