#Merges the polls table with the threads table
#Date: 10/8/2015

ALTER TABLE `threads` ADD `ispoll` INT(1) NOT NULL DEFAULT '0', ADD `question` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,ADD `multivote` INT(1) NOT NULL DEFAULT '0',ADD `changeable` INT(1) NOT NULL DEFAULT '0';UPDATE threads,polls SET threads.ispoll=1, threads.question=polls.question, threads.multivote=polls.multivote, threads.changeable=polls.changeable WHERE threads.id=polls.id;